import os
import sys
import logging
from collections import defaultdict
from natural_numbers import error_handling

def create_list_data(list_of_numbers):
    try:
        natural_numbers = []
        for line in list_of_numbers:
            line = line.split() # to deal with blank 
            if line:            # lines (ie skip them)
                line = [int(i) for i in line]
                natural_numbers.append(line)
        print(natural_numbers)
        return natural_numbers

    except IOError as exc:
        logging.error(error_handling())
        raise exc
    except ValueError as ve:
        logging.error(error_handling())
        raise ve   

if __name__ == "__main__":
    #[[2, 3, 26], [5, 4, 31], [4, 2, 19], [9, 5, 90], [3, 5, 39], [5, 6, 65], [3, 6, 43]]
    list_with_natural_numbers = ["3 6 22","5 4 31"]
    create_list_data(list_with_natural_numbers)


'''
2 3 26
5 4 31
4 2 19
9 5 90
3 5 39
5 6 65
3 6 43
'''