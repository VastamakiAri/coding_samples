*** Settings ***
Resource        natural_numbers_keywords.robot

*** Test Cases ***

Obtain Multiples From Given Natural Numbers
    [tags]          reading_and_writing_files
    Read A File     ../input.txt
    Get Multiples
    Write To File   ../wild_output.txt

Read File
    [tags]          reading_file
    Read A File  ../input.txt

Write To File
    Get Multiples From List
    Write To File  ../wild_output.txt
