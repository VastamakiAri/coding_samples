*** Settings ***
Library	        Collections
Library         OperatingSystem			
Library         natural_numbers.py
Library         natural_numbers_library.py

*** Variables ***
  
#${argv1}    input.txt
#${argv2}    wild_output.txt

*** Keywords ***

Read A File
    [Arguments]          ${argv1}
    ${NATURAL_NUMBERS}   Read File  ${argv1}
    Log                  ${NATURAL_NUMBERS}
    ${file}              Get file   ${argv1}
    Should Not Be Empty  ${file}
    Log                  ${file}
    #Should Be Equal      ${read_output}  ${file}
    Set Test Variable    ${NATURAL_NUMBERS}
    
Get Multiples
    ${RESULTS}                          Search Multiples  ${NATURAL_NUMBERS}
    Log                                 ${RESULTS}
    Set Test Variable                   ${RESULTS}

Get Multiples From List
    @{data} =    Create List            3 2 55  5 7 72
    ${list_var}                         Create List Data  ${data}
    ${RESULTS}                          Search Multiples  ${list_var}
    Set Test Variable                   ${RESULTS}

Write To File
    [Arguments]                         ${argv2}
    ${results}                          Write Into File  ${RESULTS}  ${argv2}
    ${file}                             Get file   ${argv2}
    Should Not Be Empty                 ${file}
    Log                                 ${file}
    #Dictionary Should Contain Value     ${results}  ${file}