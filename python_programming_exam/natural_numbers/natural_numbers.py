'''
Python Programming Exam:
Theory:
Multiples of Y and X
Let’s assume that Y is 3 and X is 5, so
If we list all the natural numbers below 10 that are multiples of X or Y, we get 3, 5, 6 and 9. The
sum of these multiples is 23.

TASK:
The task is to create program that takes a file as the first command line argument. The file
content is list of numbers, with 3 numbers per row separated by space and number of rows is
undefined (can be something between 1 - infinite). First number in a row is X and the second is
Y, third one is the goal number (as in theorem 10). The program has to search all multiples of X
and Y which are below the third number, print it out to screen and also write results to file which
was given as the second command line argument.
Program should short out output file by ascending order how many multiples certain row has.

Example input file content:
2 7 26
5 8 31

Example command line command: python my_fine_program.py input.txt wild_output.txt

Example screen output and output file content:
31: 5 8 10 15 16 20 24 25 30
26: 2 4 6 7 8 10 12 14 16 18 20 21 22 24
'''

import os
import sys
import logging
from collections import defaultdict

logging.basicConfig(filename='logfile.log',level=logging.INFO)


def error_handling():
    return '{}. {}, line: {}'.format(sys.exc_info()[0],
                                    sys.exc_info()[1],
                                    sys.exc_info()[2].tb_lineno)

def read_file(argv):
    try:
        with open(argv) as f:
            natural_numbers = []
            for line in f:
                line = line.split() # to deal with blank 
                if line:            # lines (ie skip them)
                    line = [int(i) for i in line]
                    natural_numbers.append(line)
        #print(natural_numbers)
        return natural_numbers

    except IOError as exc:
        logging.error(error_handling())
        raise exc
    except ValueError as ve:
        logging.error(error_handling())
        raise ve          

def search_multiples(natural_numbers):
    results = defaultdict(list)
    try:
        for index, value in enumerate(natural_numbers):
            for i in range(len(value)-1):
                for multiplication in range(1,natural_numbers[index][2]):
                    sum = natural_numbers[index][i]*multiplication
                    if sum < natural_numbers[index][2]:
                        results[natural_numbers[index][2]].append(sum)

        results_dict = dict(results)
        for key in sorted(results_dict.keys(), reverse=True):
            print("{}: {}".format(key, results_dict[key]))

        return results_dict

    except KeyError as ke:
        logging.error(error_handling())
        raise ke 
    except IndexError as ie:
        logging.error(error_handling())
        raise ie 

def write_into_file(results, argv):
    try:
        with open(argv, 'w') as f:
            for key in sorted(results.keys(), reverse=True):
                f.write("{}: {}\n".format(key, results[key]))
            return results

    except IOError as exc:
        logging.error(error_handling())
        raise exc  

def main(argv):
    if len(argv) < 3:
        sys.stderr.write('Usage: .\\python natural_numbers.py input.txt wild_output.txt')
        return 1

    else:
        natural_numbers = read_file(argv[1])
        result = search_multiples(natural_numbers)
        write_into_file(result, argv[2])

if __name__ == '__main__':
    sys.exit(main(sys.argv))     