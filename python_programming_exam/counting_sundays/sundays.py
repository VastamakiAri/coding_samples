'''
TASK:
The task is to create program that takes two dates as an input from user via user interface. The
programs task is to count amount how many Sundays fell between these dates. The program
should ask user the option shall it print the information to screen, file or both. Program should
also be able to output information in ascending and descending format.
Program should be able start counting from 1 Jan 1900 and reach out to 1.1.2500

You are given the following information, but you may prefer to do some research for yourself.
1 jan 1900 was a Monday
Thirty days has September, April, June and November.
All the rest have thirtyone,
Saving February alone,
Which has twentyeight, rain or shine.
And on leap years, twentynine.
A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400
'''

import datetime

def main():
    dates_to_write = []

    date1 = str(input("Give me the first date: "))
    day, month, year = map(int, date1.split('.'))
    loop1 = datetime.date(year, month, day)
    start = loop1
        
    date2 = str(input("Give me the second date: "))
    day, month, year = map(int, date2.split('.'))
    loop2 = datetime.date(year, month, day)
    end = loop2

    output_style = str(input("Would you like to get information on screen(s), in file(f) or both(b)? "))
    order = str(input("Do you want this information in ascending(a) or descending(d) order: "))

    print("\nSundays between dates {} and {} were:".format(loop1, loop2))

    if (order == 'd'):
        swap_order = loop2
        loop2 = loop1
        loop1 = swap_order

    while (loop1 != loop2):

        if (loop1.weekday() == 6):
        
            if (output_style == 's' or output_style == 'b'):
                print("{}".format(loop1))

            if (output_style == 'f' or output_style == 'b'):
                dates_to_write.append(loop1)

        if (order == 'a'):
            loop1 += datetime.timedelta(days=1)

        elif (order == 'd'):
            loop1 -= datetime.timedelta(days=1)

    if (output_style == 'f' or output_style == 'b'):
        save_dates_to_file(dates_to_write, start, end)



def save_dates_to_file(dates, start, end):

    try: 
        with open('dates.txt', 'w') as open_file:
            open_file.write("Sundays between dates {} and {} were:\n".format(start, end))
            for sundays in dates:
                open_file.write(str(sundays) + "\n")

    except IOError as exc:
        raise IOError("%s: %s" % ('dates.txt', exc.strerror))

if __name__ == '__main__':
    main()


