#The task is to write a program, which plays sober (read: flawless!) Martti
#Suosalo game with the given number of players and turns.

#The program shall take the number of players and turns as parameters
#(command line or compile-time constants). Then it shall print out the course of
#the game: which player shouted what.

#Constraints:
#- The game has 1 or more turns, and 1 or more players.
#- There shall be no artifical constraints, except machine integer size.
#- Although the real life game is perhaps interesting only with 3 to 10 players,
#  the cases of 1, 2 and >10 players are well defined, and should be handled.

#todo the sum of the digits is seven portion isn't working properly

def game(p, r):


#Error checking in case someone inputs less than 2 players
    if p < 2:
        raise ValueError("Needs more than 1 player")
#Error check in case someone inputs less than 1 round
    if r < 1:
        raise ValueError("Rounds can't be 0")
    

    player_list = []

    for i in range(1,p+1):
        player_list.append(i)

#variable y is used for iterating through players
#variable i determines which round it is
#direction changes the direction every 11th round
    y = 0
    i = 1
    direction = 1

    print("Players: %d \t Rounds: %d" %(p,r))

#loop until rounds marked by r are over.
#first two if check for the edge cases of the player_list so that program can loop players
    while r > 0:
        
        if(y >= p):
           y = 0

        if (y < 0):
           y = p-1

        #print(int(y)+int(i)+1)



#checks if rounds are divisible by 7
        if(i % 7 == 0):
            print(("P: %s \t %s" %(player_list[y], "Martti Suosalo")))
#checks if rounds + player equals 7 (17 for example)
        elif (((i+y)%10) == 7):
           print(("P: %s \t %s" %(player_list[y], "Martti Suosalo")))
#checks if 7 is one of it's digits
        elif ((i%10) == 7 or i/10 == 7):
           print(("P: %s \t %s" %(player_list[y], "Martti Suosalo")))
#otherwise iterate normally
        else:
            print("P: %s \t %d" %(player_list[y], i))
#after i is over 10 rounds this if tests if direction will be changed. If variable direction is odd it goes clockwise and even causes it to go counter clockwise
        if (i >= 10 and i % 11*direction == 0):
            direction += 1
        
        if direction % 2 == 0:
            y -= 1       
        else:
            y += 1
#increment rounds and while loop condition
        i += 1
        r -= 1  
            

if __name__ == '__main__':
#init the program by giving it player count and round amount
    players = int(input("Give amount of players: "))
    rounds = int(input("Amount of rounds? "))

    game(players,rounds)