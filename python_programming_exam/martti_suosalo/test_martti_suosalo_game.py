import unittest
from martti_suosalo_game import game

class TestGame(unittest.TestCase):

    def test_value_errors(self):
        """test if p<2 and r<1"""
        self.assertRaises(ValueError , game, 1, 20)
        self.assertRaises(ValueError , game, 0, 300)
        self.assertRaises(ValueError , game, 25,0)
       # self.assertRaises(ValueError , game, [], 20)
        
    def test_sample_generation(self):
        """Example runs"""
        game(5, 20)
        game(100,1000)
        game (200,8000)
        #game(2000, 120000)
        #game(20, 5000111222) takes >10 mins to run

    #def test_example1(self):
    #    r = 10
    #    self.assertEqual(None, game(5, 20))
    #    self.assertEqual(None, game(10, r))

if __name__ == '__main__':
    unittest.main()