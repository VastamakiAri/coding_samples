# -----------------------------------------------------------------------
# Simple program that prints temperature of a city
# .kv file used for layout
# Api documentation: https://openweathermap.org/guide
# Kivy documentation: https://kivy.org/doc/stable/guide/basic.html
# -----------------------------------------------------------------------

# To-do #
#-------------------------------------------------------------------------
# Make UI look more appealing
# Version that works on mobile devices
# Make it refresh every hour
#-------------------------------------------------------------------------
import os
import sys
import requests
import logging
import kivy
kivy.require('1.10.1')
from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.boxlayout import BoxLayout
from kivy.properties import StringProperty
from kivy.config import Config
Config.set('graphics', 'width', '400')
Config.set('graphics', 'height', '500')

# This class is abstracted into .kv file
class WeatherApp(App):
    pass

# Most of the logic is run inside this class using requests to fetch the
# JSON data from openweathermap.org. Properties are used to link variables
# into .kv file and logging is used on command line level. Sys library is used
# to print a custom error message. 
# Previous call is remembered on opening with a text file.

class WeatherRoot(BoxLayout):
    location=StringProperty()
    conditions=StringProperty()
    temp=StringProperty()
    wind=StringProperty()
    temp_max=StringProperty()    
    humidity=StringProperty()
    previous_call=StringProperty()

    if os.path.isfile("prev_city.txt"):
        with open("prev_city.txt","r") as f:
            w = f.read()
            previous_call = w
    else:
        previous_call = ""

    def request_call(self, location):
        try:
            r = requests.get('http://api.openweathermap.org/data/2.5/weather?q='+location+'&units=metric&APPID=4bc6c439b0c92c82fddca60910f6ce5e')

        except Exception:
            logging.error(self.error_handling())

        weather = r.json()
        return weather

    def weather(self, city):
        weather_data = self.request_call(city)
        m_symbol = u'\xb0' + 'C'

        try:

            self.location='{0}, {1}'.format(weather_data['name'], weather_data['sys']['country'])
            self.conditions='Weather: {0}'.format(weather_data['weather'][0]['description'])
            self.temp='Current temperature: {0} {1}'.format(weather_data['main']['temp'], m_symbol)
            self.wind="Wind speed: {0} m/s".format(weather_data['wind']['speed'])
            self.temp_max='Temperature range: {0} {1} - {2} {3}'.format(weather_data['main']['temp_min'], m_symbol, weather_data['main']['temp_max'], m_symbol)
            self.humidity='Humidity: {0} %'.format(weather_data['main']['humidity'])

            print ("----------------------------------------")
            print (self.location+"\n"+self.conditions+"\n"+self.temp+"\n"+self.temp_max+"\n"+self.wind+"\n"+self.humidity)
            print ("----------------------------------------")

            with open("prev_city.txt","w") as f:
                f.write(city)
        
        except Exception:
            logging.error(self.error_handling())
            if KeyError:
                print("Error handling weather data, not a valid city name")
           
    def error_handling(self):
        return '{}. {}, line: {}'.format(sys.exc_info()[0],
                                        sys.exc_info()[1],
                                        sys.exc_info()[2].tb_lineno)


if __name__ == '__main__':
    WeatherApp().run()
