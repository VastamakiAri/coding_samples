import kivy
kivy.require('1.10.1')
from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.boxlayout import BoxLayout
from kivy.config import Config
Config.set('graphics', 'width', '420')
Config.set('graphics', 'height', '500')

# To change dynamically after window has been created
#from kivy.core.window import Window
#Window.size = (400, 500)

class CalculatorRoot(BoxLayout):
    
    def calculate(self, calculate):
        if calculate:
            try:
                self.display.text = str(eval(calculate))
            except Exception:
                self.display.text = "ERROR!"

class CalculatorApp(App):
    #def build(self):
    #    return CalculatorRoot
    pass

if __name__ == '__main__':
    CalculatorApp().run()
