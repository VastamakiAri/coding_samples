import kivy
kivy.require('1.10.1')

from kivy.app import App
from kivy.uix.label import Label

from kivy.uix.boxlayout import BoxLayout
from kivy.properties import ObjectProperty,ListProperty,NumericProperty,StringProperty #importing kivy properties 
from kivy.uix.listview import ListItemButton

class MyApp(App):
    #def build(self):
    #    return HelloRoot(info='Hello world')
    pass

class HelloRoot(BoxLayout):
    txt_inpt = ObjectProperty(None)
    label_wid = StringProperty('My label before button press')
    info = StringProperty()

    def check_status(self, btn):
        print('button state is: {state}'.format(state=btn.state))
        #print('text input text is: {txt}'.format(txt=self.txt_inpt))
    
    def do_action(self):
        self.label_wid = 'My label after button press'
        self.info = 'World!'

if __name__ == '__main__':
    MyApp().run()
