'''
The following function counts the number of decimal digits in a positive integer
but only if there's no remainder or remainder is 5
'''
import sys

def num_zero_and_five_digits(n):
    count = 0
    while n > 0:
        digit = n % 10
        if digit == 0 or digit == 5:
            count = count + 1
        n = n // 10
    return count

'''
The following function counts the number of decimal digits in a positive integer:
'''

def num_digits(n):
    count = 0
    while n != 0:
        count = count + 1
        n = n // 10
    return count



'''
Hand made unit test
'''

def test(did_pass):
    """  Print the result of a test.  """
    linenum = sys._getframe(1).f_lineno   # Get the caller's line number.
    if did_pass:
        msg = "Test at line {0} ok.".format(linenum)
    else:
        msg = ("Test at line {0} FAILED.".format(linenum))
    print(msg)



def test_suite():
    """ Run the suite of tests for code in this module (this file).
    """
    test(num_zero_and_five_digits(1055030250) == 7)
    test(num_zero_and_five_digits(10550302) == 5)
    test(num_digits(0) == 1) #Division by zero
    test(num_digits(1) == 1)


if __name__ == '__main__':
    test_suite()