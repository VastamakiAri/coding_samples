from __future__ import print_function # Just in case there's some problems
import string
from testing import test
import sys
import time

'''----------Chapter 14 - List algorithms-------------'''

#--------------------------------------------------------------------------#
# File reading to strings latter function nearly identical, 
# just adds the extra step of removing punctuations
#--------------------------------------------------------------------------#
def load_words_from_file(filename):
    """ Read words from filename, return list of words. """
    f = open(filename, "r")
    file_content = f.read()
    f.close()
    wds = file_content.split()
    return wds

def get_words_in_book(filename):
    """ Read a book from filename, and return a list of its words. """
    f = open(filename, "r")
    content = f.read()
    f.close()
    wds = text_to_words(content)
    return wds
#--------------------------------------------------------------------------#


#--------------------------------------------------------------------------#
# maketrans() and translate() to remove punctuations, etc.
#--------------------------------------------------------------------------#
def text_to_words(the_text):
    """ return a list of words with all punctuation removed,
        and all in lowercase.
    """
    # In python 3 you can use <string_name>.maketrans() but in 2.7 we have to use string.maketrans()
    my_substitutions = string.maketrans(
      # If you find any of these
      "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!\"#$%&()*+,-./:;<=>?@[]^_`{|}~'\\",
      # Replace them by these
      "abcdefghijklmnopqrstuvwxyz                                          ")

    # Translate the text now.
    cleaned_text = the_text.translate(my_substitutions)
    wds = cleaned_text.split()
    return wds
#--------------------------------------------------------------------------#


#--------------------------------------------------------------------------#
# The actual search functions 
#--------------------------------------------------------------------------#
def find_unknown_words_linear(vocab, wds):
    """ Return a list of words in wds that do not occur in vocab """
    result = []
    for w in wds:
        if (search_linear(vocab, w) < 0):
            result.append(w)
    return result

def find_unknown_words_binary(vocab, wds):
    """ Return a list of words in wds that do not occur in vocab """
    result = []
    for w in wds:
        if (search_binary(vocab, w) < 0):
            result.append(w)
    return result

def search_linear(xs, target):
    """ Find and return the index of target in sequence xs """
    for (i, v) in enumerate(xs): # Could do this with range() for example
       if v == target:
           return i
    return -1

def search_binary(xs, target):
    """ Find and return the index of key in sequence xs """
    lb = 0
    ub = len(xs)
    while True:
        if lb == ub:   # If region of interest (ROI) becomes empty
           return -1

        # Next probe should be in the middle of the ROI
        mid_index = (lb + ub) // 2

        # Fetch the item at that position
        item_at_mid = xs[mid_index]

        # print("ROI[{0}:{1}](size={2}), probed='{3}', target='{4}'"
        #       .format(lb, ub, ub-lb, item_at_mid, target))

        # How does the probed item compare to the target?
        if item_at_mid == target:
            return mid_index      # Found it!
        if item_at_mid < target:
            lb = mid_index + 1    # Use upper half of ROI next time
        else:
            ub = mid_index        # Use lower half of ROI next time
#--------------------------------------------------------------------------#
 

#--------------------------------------------------------------------------# 
# main() start
#--------------------------------------------------------------------------#
if __name__ == '__main__':

    bigger_vocab = load_words_from_file("vocab.txt")
    print("There are {0} words in the vocab, starting with\n{1}\n"
            .format(len(bigger_vocab), bigger_vocab[:6]))

    book_words = get_words_in_book("alice_in_wonderland.txt")
    print("There are {0} words in the book, the first 100 are\n{1}\n"
           .format(len(book_words), book_words[:100]))

    # Timing how long it takes to iterate and compare linearly through the whole alice_in_wonderland.txt
    t0 = time.clock()
    missing_words = find_unknown_words_linear(bigger_vocab, book_words)
    t1 = time.clock()
    print("There are {0} unknown words.".format(len(missing_words)))
    print("That took {0:.4f} seconds.\n".format(t1-t0))

    t0 = time.clock()
    missing_words = find_unknown_words_binary(bigger_vocab, book_words)
    t1 = time.clock()
    print("There are {0} unknown words.".format(len(missing_words)))
    print("That took {0:.4f} seconds.".format(t1-t0))
#--------------------------------------------------------------------------#
# main() end
#--------------------------------------------------------------------------#


# Linear search tests #
''' 
vocab = ["apple", "boy", "dog", "down",
                          "fell", "girl", "grass", "the", "tree"]
book_words = "the apple fell from the tree to the grass".split()
test(find_unknown_words(vocab, book_words) == ["from", "to"])
test(find_unknown_words([], book_words) == book_words)
test(find_unknown_words(vocab, ["the", "boy", "fell"]) == [])
'''

# Binary search tests#
'''
xs = [2,3,5,7,11,13,17,23,29,31,37,43,47,53]
test(search_binary(xs, 20) == -1)
test(search_binary(xs, 99) == -1)
test(search_binary(xs, 1) == -1)
test(search_binary(xs, 2) == 0)
test(search_binary(xs, 11) == 4)
for (i, v) in enumerate(xs):
    test(search_binary(xs, v) == i)
'''