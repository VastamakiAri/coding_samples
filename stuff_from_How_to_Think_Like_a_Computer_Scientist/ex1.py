#------------------------------------------------------------------------------------------------------------------------------------------------------
# A = P(1+((r/n)**nt)) 
# Write a Python program that assigns the principal amount 
# of $10000 to variable P, assign to n the value 12,
# and assign to r the interest rate of 8%. 
# Then have the program prompt the user for the number of years t 
# that the money will be compounded for. 
# Calculate and print the final amount after t years.
#------------------------------------------------------------------------------------------------------------------------------------------------------

P = 10000
n = 12.0
r = 0.08

# t = (P*((1+(r//n))**n)))/A)

t = int(raw_input("\nGive me the number of years money will be compounded for: "))
nt = n*t
A = P*((1+(r/n))**nt)

print("After {} years, final compounded amount is {}\n".format(t,A))
