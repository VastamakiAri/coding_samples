'''
"7777...8?!??!", exclaimed Bob, "I missed it again! Argh!" Every time there's an interesting number coming up, he notices and then promptly forgets. 
Who doesn't like catching those one-off interesting mileage numbers?

Let's make it so Bob never misses another interesting number. We've hacked into his car's computer, and we have a box hooked up that reads mileage numbers.
We've got a box glued to his dash that lights up yellow or green depending on whether it receives a 1 or a 2 (respectively).

It's up to you, intrepid warrior, to glue the parts together. Write the function that parses the mileage number input, and returns a 2 if the number is "interesting" (see below), 
a 1 if an interesting number occurs within the next two miles, or a 0 if the number is not interesting.

Note: In Haskell, we use No, Almost and Yes instead of 0, 1 and 2.

"Interesting" Numbers
Interesting numbers are 3-or-more digit numbers that meet one or more of the following criteria:

Any digit followed by all zeros: 100, 90000
Every digit is the same number: 1111
The digits are sequential, incementing†: 1234
The digits are sequential, decrementing‡: 4321
The digits are a palindrome: 1221 or 73837
The digits match one of the values in the awesome_phrases array
† For incrementing sequences, 0 should come after 9, and not before 1, as in 7890.
‡ For decrementing sequences, 0 should come after 1, and not before 9, as in 3210.

So, you should expect these inputs and outputs:

# "boring" numbers
is_interesting(3, [1337, 256])    # 0
is_interesting(3236, [1337, 256]) # 0

# progress as we near an "interesting" number
is_interesting(11207, []) # 0
is_interesting(11208, []) # 0
is_interesting(11209, []) # 1
is_interesting(11210, []) # 1
is_interesting(11211, []) # 2

# nearing a provided "awesome phrase"
is_interesting(1335, [1337, 256]) # 1
is_interesting(1336, [1337, 256]) # 1
is_interesting(1337, [1337, 256]) # 2
Error Checking
A number is only interesting if it is greater than 99!
Input will always be an integer greater than 0, and less than 1,000,000,000.
The awesomePhrases array will always be provided, and will always be an array, but may be empty. (Not everyone thinks numbers spell funny words...)
You should only ever output 0, 1, or 2.
'''
# I should make this code cleaner someday and add some methods
import unittest

class MileageNumbersTests(unittest.TestCase):

    def test_awesome_number(self):
        # Awesome number
        self.assertEqual(is_interesting(3, [1337, 256]), 0)
        self.assertEqual(is_interesting(1336, [1337, 256]), 1)
        self.assertEqual(is_interesting(1337, [1337, 256]), 2)
        self.assertEqual(is_interesting(256, [1337, 256, 376006]), 2)
        
    def test_palindromes(self):
        # Palindromes
        self.assertEqual(is_interesting(11208, []), 0)
        self.assertEqual(is_interesting(11209, []), 1)
        self.assertEqual(is_interesting(11211, [1337, 256]), 2)

    def test_trailing_zeroes(self):
        # Trailing zeroes
        self.assertEqual(is_interesting(100, []), 2)
        self.assertEqual(is_interesting(5000, []), 2)
        self.assertEqual(is_interesting(7010, []), 0)
        self.assertEqual(is_interesting(1100, []), 0)
        self.assertEqual(is_interesting(98, []), 1)
        self.assertEqual(is_interesting(99, []), 1)
        self.assertEqual(is_interesting(799999, []), 1)
        self.assertEqual(is_interesting(6998, []), 1)

    def test_same_digit(self):
        # Same digit
        self.assertEqual(is_interesting(1111, []), 2)
        self.assertEqual(is_interesting(110111, []), 0)

    def test_sequential_asc(self):
        # Sequential ascending
        self.assertEqual(is_interesting(12345, []), 2)
        self.assertEqual(is_interesting(12445, []), 0)
        self.assertEqual(is_interesting(12346, []), 0)
        self.assertEqual(is_interesting(122, []), 1)

    def test_sequential_desc(self):
        # Sequential descending
        self.assertEqual(is_interesting(4321, []), 2)
        self.assertEqual(is_interesting(554321, []), 0)
        self.assertEqual(is_interesting(43212, []), 0)
        self.assertEqual(is_interesting(3208, []), 1)

    def test_sequential_with_zero(self):
        # Sequential with 0
        self.assertEqual(is_interesting(3210, []), 2) 
        self.assertEqual(is_interesting(7890, []), 2)        

def is_interesting(number, awesome_phrases):

    for _ in range(2):
        if (100 - number) < 3 and (100 - number) > 0:
            number+=1
            return 1

    if (number < 100) or (number < 0) or (number > 1000000000):
        print("number cannot be less than 100, negative or over 1,000,000,000!")
        return 0

    lst = [int(i) for i in str(number)]
    number2 = number
    number3 = number
    number4 = number

    # Same digits    
    for _ in lst:
        if(len(set(lst))==1):
            return 2        
    '''
    # Sequential ascending
    for i in range(3):
        lst3 = [int(i) for i in str(number3)]
        for j in range(len(lst3)):
            #print(lst3)
            #print(i)
            if (j == len(lst3)-1 and i == 0):
                return 2
            if (j == len(lst3)-1 and i != 0):
                return 1
            if lst3[j] == 9 and lst3[j+1] == 0:
                pass 
            elif (lst3[j+1] - lst3[j]) != 1:
                break
        number3+=1
    '''
    # Sequential ascending
    for i in range(3):
        lst3 = [int(i) for i in str(number3)]
        for index, _ in enumerate(lst3[:]):
            #print(value)
            #print(i)
            if (index == len(lst3)-1 and i == 0):
                return 2
            if (index == len(lst3)-1 and i != 0):
                return 1
            if lst3[index] == 9 and lst3[index+1] == 0:
                pass 
            elif (lst3[index+1] - lst3[index]) != 1:
                break
        number3+=1

    # Sequential descending
    for i in range(3):
        lst4 = [int(i) for i in str(number4)]
        for index, _ in enumerate(lst4[::-1]):
            #print(lst[index])
            #print(f"index: {index}")
            if index == len(lst4)-1 and i== 0:
                return 2
            if (index == len(lst4)-1 and i != 0):
                return 1
            if lst4[index] - lst4[index+1] != 1:
                break    
        number4+=1

    # Trailing zeroes
    if lst[0] != 0:
        for i in range(3):
            sum = 0
            lst2 = [int(i) for i in str(number2)]
            #print(lst2)
            for j in lst2[1:]:
                sum += j
            if (sum == 0 and i == 0):
                return 2
            elif(sum == 0 and i != 0):
                return 1
            number2+=1

    # Awesome phrases
    for value in awesome_phrases:
        if value == number:
            return 2

    for value in awesome_phrases:
        if (number - value) < 2 and (number - value) != 0:
            return 1
    
    # Palindrome
    if (int(str(number) == str(number)[::-1])):
        return 2
    else:
        for i in range(2):
            number += 1
            if (int(str(number) == str(number)[::-1])):  
                        return 1

    # if number is still boring return 0
    return 0

    
if __name__ == "__main__":
    unittest.main(verbosity=3)


'''
def is_incrementing(number): return str(number) in '1234567890'
def is_decrementing(number): return str(number) in '9876543210'
def is_palindrome(number):   return str(number) == str(number)[::-1]
def is_round(number):        return set(str(number)[1:]) == set('0')

def is_interesting(number, awesome_phrases):
    tests = (is_round, is_incrementing, is_decrementing,
             is_palindrome, awesome_phrases.__contains__)
       
    for num, color in zip(range(number, number+3), (2, 1, 1)):
        if num >= 100 and any(test(num) for test in tests):
            return color
    return 0
'''