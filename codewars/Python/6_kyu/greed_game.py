from collections import Counter
import unittest
'''
Greed is a dice game played with five six-sided dice. Your mission, should you choose to accept it, is to score a throw according to these rules. You will always be given an array with five six-sided dice values.

 Three 1's => 1000 points
 Three 6's =>  600 points
 Three 5's =>  500 points
 Three 4's =>  400 points
 Three 3's =>  300 points
 Three 2's =>  200 points
 One   1   =>  100 points
 One   5   =>   50 point
A single die can only be counted once in each roll. For example, a "5" can only count as part of a triplet (contributing to the 500 points) or as a single 50 points, but not both in the same roll.

Example scoring

 Throw       Score
 ---------   ------------------
 5 1 3 4 1   50 + 2 * 100 = 250
 1 1 1 3 1   1000 + 100 = 1100
 2 4 4 5 4   400 + 50 = 450
 '''

class greedTests(unittest.TestCase):

    def test_greed(self):
        self.assertEqual(score([1, 5, 3, 1, 8]), 250)
        self.assertEqual(score([2, 3, 4, 6, 2]), 0)
        self.assertEqual(score([4, 4, 4, 3, 3]), 400)
        self.assertEqual(score([2, 4, 4, 5, 4]), 450)
        self.assertEqual(score([5, 5, 5, 3, 3]), 500)
        self.assertEqual(score([1, 1, 1, 1, 3]), 1100)
        self.assertEqual(score([1, 1, 1, 1, 5]), 1150)
        self.assertEqual(score([5, 5, 5, 5, 1]), 650)
        self.assertEqual(score([1, 5, 1, 5, 1]), 1100)

# since we are only using 5 dice it's impossible to score triples twice with same number
def score(dice):
    score = 0
    rolls = Counter(dice)

    for number,total_rolls in rolls.most_common():
        if number == 1 and total_rolls >=3:
            score += (number*1000) + ((total_rolls-3)*100)
        elif number == 1:
            score += total_rolls*100
        if number == 5 and total_rolls >=3:
            score+=((total_rolls-3)*number)*10
        elif number == 5:
            score+= (number*total_rolls)*10
        if (number is not 1) and (total_rolls >= 3):
            score += number*100
    return score
        
if __name__ == '__main__':
    unittest.main(verbosity=3)



