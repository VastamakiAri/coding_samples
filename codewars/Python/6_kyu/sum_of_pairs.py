from testing import test
# This is not complete yet, trying to do this O(n) way.
# Implementation of memoization needed to clear the runtime test

#import sys
#import logging

'''
Given a list of integers and a single sum value, 
return the first two values (parse from the left please) in order of appearance 
that add up to form the sum.
'''
ef_cache = {}

def sum_pairs(ints, s):

    #if s in ef_cache:
        #print (ef_cache[s])
        #return ef_cache[s]

    ints_dict = dict(enumerate(ints))
    ints_copy = ints[1:]
    #print(ints_copy)
    answer = []
    i = 0
    #indeces = -1
    #index2 = -1

    while i < len(ints_dict):
            #l1= [1, 4, 8, 7, 3, 15]
            #print(i, ints_dict[i], ints_copy[i])
            print ((ints_copy[i:i+2]))
            if sum(ints_copy[i:i+2]) == s:
                answer.extend([ints_copy[i],ints_copy[i+1]])
                #print(answer)
                ef_cache[s] = answer
                print(ef_cache)
                return answer

            else:
                i += 1

if __name__ == '__main__':

    l1= [1, 4, 8, 7, 3, 15]
    l2= [1, -2, 3, 0, -6, 1]
    l3= [20, -13, 40]
    l4= [1, 2, 3, 4, 1, 0]
    l5= [10, 5, 2, 3, 7, 5]
    l6= [4, -2, 3, 3, 4]
    l7= [0, 2, 0]
    l8= [5, 9, 13, -3]

    test(sum_pairs(l1, 8) == [1, 7])
    #test(sum_pairs(l5, 10) == [3, 7])
    #test(sum_pairs(l8, 10) == [13,-3])
    #test(sum_pairs(l7, 0) == [0,0])