import unittest
# Importing all the kyu6 libraries in this folder
from missing_letter import find_missing_letter
from narcissistic_number import narcissistic
from unique_in_order import unique_in_order
from valid_phone_number import validPhoneNumber

class kyu6_tests(unittest.TestCase):

    def test_missing_letter(self):
        self.assertEqual(find_missing_letter(['a','b','c','d','f']), 'e', "Expecting 'e' as result")
        self.assertEqual(find_missing_letter(['O','Q','R','S']), 'P', "Expecting 'P' as result")
        self.assertEqual(find_missing_letter(['1','3','4','5']), '2', "Expecting '2' as result")

    def test_narcissistic_number(self):
        self.assertTrue(narcissistic(7))
        self.assertTrue(narcissistic(371))
        self.assertFalse(narcissistic(122))
        self.assertFalse(narcissistic(4887), "Expecting 4887 to be false")

    def test_unique_in_order(self):
        self.assertEqual(unique_in_order('AAAABBBCCDAABBB'), ['A','B','C','D','A','B'])
        self.assertEqual(unique_in_order('ABBCcAD'), ['A', 'B', 'C', 'c', 'A', 'D'])
        self.assertEqual(unique_in_order(''), [])
        self.assertEqual(unique_in_order([1,2,2,3,3]), [1,2,3])

    def test_valid_phone_number(self):
        self.assertTrue(validPhoneNumber("(123) 456-7890"))
        self.assertFalse(validPhoneNumber("(1111)555 2345"))
        self.assertFalse(validPhoneNumber("(098) 123 4567"))
        self.assertFalse(validPhoneNumber("(123) 456-7890abc"))
        self.assertFalse(validPhoneNumber("abc(123) 456-7890"))

if __name__ == '__main__':
    unittest.main(verbosity=3)
    #fooSuite = unittest.TestLoader().loadTestsFromTestCase(Kyu8TestCases)


'''
    # preparing to test
    def setUp(self):
        """ Setting up for the test """
        print "FooTest:setUp_"
     
    # ending the test
    def tearDown(self):
        """Cleaning up after the test"""
        print "FooTest:tearDown_"
'''