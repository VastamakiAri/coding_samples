from testing import test
'''
['a','b','c','d','f'] -> 'e'
['O','Q','R','S'] -> 'P'
(Use the English alphabet with 26 letters!)
'''
# In this test every missing letter is ordered so you can find the missing one
# by testing if adding one to the current index equals the next index or not
def find_missing_letter(chars):
    return [chr(ord(chars[i]) + 1) for i in range(len(chars) -1) if ord(chars[i]) + 1 != ord(chars[i + 1])][0]

def main():
    test(find_missing_letter(['a','b','c','d','f']) == 'e')
    test(find_missing_letter(['O','Q','R','S']) == 'P')
    test(find_missing_letter(['1','3','4','5']) == '2')

if __name__ == '__main__':
    main()

# Another way
'''
def find_missing_letter(chars):
    n = 0
    while ord(chars[n]) == ord(chars[n+1]) - 1:
        n += 1
    return chr(1+ord(chars[n]))
'''