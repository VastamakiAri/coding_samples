'''
Your task is to sort a given string. Each word in the string will contain a single number. 
This number is the position the word should have in the result.

Note: Numbers can be from 1 to 9. So 1 will be the first word (not 0).

If the input string is empty, return an empty string. 
The words in the input String will only contain valid consecutive numbers.

Examples
"is2 Thi1s T4est 3a"  -->  "Thi1s is2 3a T4est"
"4of Fo1r pe6ople g3ood th5e the2"  -->  "Fo1r the2 g3ood 4of th5e pe6ople"
""  -->  ""
'''
import unittest
import re

class OrderByNumbersTests(unittest.TestCase):

    def test_sorted_by_number_in_words(self):
            self.assertEqual(order("is2 Thi1s T4est 3a"), "Thi1s is2 3a T4est")
            self.assertEqual(order("4of Fo1r pe6ople g3ood th5e the2"), "Fo1r the2 g3ood 4of th5e pe6ople")
            self.assertEqual(order(""), "")

def order(sentence):

    if not sentence:
        return sentence
        
    else:
        se_split = list(sentence.split(" "))
        se_list = sorted(se_split, key=lambda x: int(re.sub(r'\D', '', x)))
        se_string = ' '.join(map(str, se_list))
        print(se_string) 
        return se_string

if __name__ == "__main__":
    unittest.main(verbosity=3)