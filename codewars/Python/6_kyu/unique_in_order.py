'''
Implement the function unique_in_order which takes as argument a sequence and returns a list of items without any elements with the same value next to each other and preserving the original order of elements.

For example:

unique_in_order('AAAABBBCCDAABBB') == ['A', 'B', 'C', 'D', 'A', 'B']
unique_in_order('ABBCcAD')         == ['A', 'B', 'C', 'c', 'A', 'D']
unique_in_order([1,2,2,3,3])       == [1,2,3]
'''

from testing import test

def unique_in_order(iterable):
    result = []
    prev = None
    for char in iterable[0:]:
        if char != prev:
            result.append(char)
            prev = char
    return result

if __name__ == '__main__':
    test(unique_in_order('AAAABBBCCDAABBB') == ['A','B','C','D','A','B'])
    test(unique_in_order('ABBCcAD') == ['A', 'B', 'C', 'c', 'A', 'D'])   



'''
def unique_in_order(iterable):
    sort= []
    
    if type(iterable) == bool:
        return set(iterable)

    if iterable in (None, ''):
        return []

    for i in range(len(iterable)-1):
        if iterable[i] != iterable[i+1]:
            sort.append(iterable[i])
    sort.append(iterable[len(iterable)-1])
    
    return sort
'''