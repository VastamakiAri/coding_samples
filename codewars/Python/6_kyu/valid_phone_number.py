import re
from testing import test

'''
Write a function that accepts a string, and returns true if it is in the form of a phone number. 
Assume that any integer from 0-9 in any of the spots will produce a valid phone number.

Only worry about the following format:
(123) 456-7890 (don't forget the space after the close parentheses) 
'''

# Using regex to find if number is proper format
def validPhoneNumber(phoneNumber):
    return bool(re.search(r'\B\(\d+\)\s\d+-\d+\b', phoneNumber))
    '''
    regexp = re.compile(r'\(\d+\)\s\d+-\d+')
    matches = pattern.finditer(phoneNumber)

    for match in matches:
        print(match)
    '''

def main():
    test(validPhoneNumber("(123) 456-7890") == True)
    test(validPhoneNumber("(1111)555 2345") == False)
    test(validPhoneNumber("(098) 123 4567") == False)
    test(validPhoneNumber("(123) 456-7890abc") == False)
    test(validPhoneNumber("abc(123) 456-7890") == False)    

if __name__ == '__main__':
    main()