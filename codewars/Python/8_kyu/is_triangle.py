# Solution to this problem requires you to know that sum of two sides has to be greater than third side
import sys
from testing import test

def is_triangle(a, b, c):
    return ((b+c > a) and (a + c > b) and (a+b > c))

if __name__ == '__main__':

    test(is_triangle(1, 2 ,2) == True)
    test(is_triangle(1, 2 ,3) == False)
    test(is_triangle(5, 3 ,2) == False)
    test(is_triangle(4, 5 ,4) == True)
    test(is_triangle(-1, 0 ,-1) == False)
