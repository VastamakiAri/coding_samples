import sys
from testing import test

def binary_array_to_number(arr):
    return int(('0b' + ''.join(['1' if x else '0' for x in arr])),2)

def binary_array_to_number2(arr):
  return int("".join(map(str, arr)), 2)

if __name__ == '__main__':

    test(binary_array_to_number([0, 0, 0, 1]) == 1)
    test(binary_array_to_number([0, 0, 1, 0]) == 2)
    test(binary_array_to_number([0, 1, 0, 0]) == 4)
    test(binary_array_to_number([1, 0, 0, 0]) == 8)