# Deoxyribonucleic acid (DNA) is a chemical found in the nucleus of 
# cells and carries the "instructions" for the development and functioning of
# living organisms.

# In DNA strings, symbols "A" and "T" are complements of each other, as "C" and "G".
# You have function with one side of the DNA (string, except for Haskell); you 
# need to get the other complementary side. 
# DNA strand is never empty or there is no DNA at all (again, except for Haskell).

import sys
from testing import test

def DNA_strand(dna):
    result = []
    
    for i in dna:
        if i == "A":
            result.append("T")
            
        elif i == "T":
            result.append("A")
        
        elif i == "C":
            result.append("G")
            
        elif i == "G":
            result.append("C")

    return "".join(result)

# Shorter and better ones taken for reference

'''
import string
def DNA_strand(dna):
    return dna.translate(string.maketrans("ATCG","TAGC"))
    # Python 3.4 solution || you don't need to import anything :)
    # return dna.translate(str.maketrans("ATCG","TAGC"))
'''
'''
def DNA_strand(dna):
    reference = { "A":"T",
                  "T":"A",
                  "C":"G",
                  "G":"C"
                  }
    return "".join([reference[x] for x in dna])
'''

if __name__ == '__main__':

    test(DNA_strand("AAAA") == "TTTT")
    test(DNA_strand("ACGGGCTTAAAGTCGGTGAAAT") == "TGCCCGAATTTCAGCCACTTTA")


