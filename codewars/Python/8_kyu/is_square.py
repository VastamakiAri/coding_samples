import sys
from testing import test

def is_square(n):
    if n < 0:
        return False
    return [True if (n**0.5).is_integer() else False][0]

if __name__ == '__main__':

    test(is_square(0) == True)
    test(is_square(2) == False)
    test(is_square(25) == True)
    test(is_square(-2) == False)
    test(is_square(3) == False)
