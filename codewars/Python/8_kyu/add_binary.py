import sys
from testing import test

def add_binary(a,b):
    return bin(a+b)[2:]

if __name__ == '__main__':

    test(add_binary(1,1) == "10")
    test(add_binary(2,2) == "100")
    test(add_binary(51,12) == "111111")