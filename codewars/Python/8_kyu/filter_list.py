import sys
from testing import test

def filter_list(l):
    result = []
    for i in l:
        if type(i) is int:
            result.append(i)
    return result

if __name__ == '__main__':

    test(filter_list([1,2,'a','b']) == [1,2])
    test(filter_list([1,'a','b',0,15]) == [1,0,15])
    test(filter_list([1,2,'aasf','1','123',123]) == [1,2,123])