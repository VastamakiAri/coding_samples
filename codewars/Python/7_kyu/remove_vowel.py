import sys
from testing import test
import string
# python 2.7 variation differs from python 3
def disemvowel(sentence):
    return sentence.translate(None, 'aeiouAEIOU')

if __name__ == '__main__':

    test(disemvowel("This website is for losers LOL!") == ("Ths wbst s fr lsrs LL!"))