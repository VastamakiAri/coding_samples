'''
You have an array of numbers.
Your task is to sort ascending odd numbers but even numbers must be on their places.

Zero isn't an odd number and you don't need to move it. If you have an empty array, you need to return it.

Example

sort_array([5, 3, 2, 8, 1, 4]) == [1, 3, 2, 8, 5, 4]
'''
import sys
from testing import test
 
def sort_array(source_array):
    sorted_odds = sorted([i for i in source_array if i%2])
    # print sorted_odds
    new_list = []
    for i in source_array:
        # print new_list
        if i%2:
            new_list.append(sorted_odds.pop(0))            
        else:
            new_list.append(i)
    print (new_list)
    return list(new_list)


if __name__ == "__main__":
    test(sort_array([5, 3, 2, 8, 1, 4]) == [1, 3, 2, 8, 5, 4])

'''
def sort_array(arr):
  odds = sorted((x for x in arr if x%2 != 0), reverse=True)
  return [x if x%2==0 else odds.pop() for x in arr]
'''