/*Given a demographics table in the following format:

** demographics table schema **

id
name
birthday
race
you need to return a table that shows a count of each race represented, ordered by the count in descending order as:

** output table schema **

race
count
*/

SELECT id, name, birthday, race
FROM demographics;

SELECT COUNT(race),race
FROM demographics
GROUP BY race
ORDER BY COUNT(race) DESC;

/*
Your task is to sort the information in the provided table 'companies' by number of employees (high to low). Returned table should be in the same format as provided:

companies table schema

id
ceo
motto
employees
Solution should use pure SQL. Ruby is only used in test cases.
*/

SELECT id, ceo, motto, employees
FROM companies
ORDER BY employees DESC;


/*Return a table with two columns (number1, number2) where the values in number1 
have been rounded down and the values in number2 have been rounded up.*/

SELECT FLOOR(number1) AS number1, CEILING(number2) AS number2
FROM decimals;

/*You'll have a table like the following:

name	greeting
Austin Gaylord	Hola que tal #4702665
Kacie Zulauf	Bienvenido 45454545 tal #470815 BD. WA470815*/

/*In this practice you'll need to extract from the greeting column the number preceeded by the # symbol and place it in a new column named user_id.

name	greeting	user_id
Austin Gaylord	Hola que tal #4702665	4702665
Kacie Zulauf	Bienvenido 45454545 tal #470815 BD. WA470815	470815
NOTE: To keep it simple assume that the iser_id will be havong varchar type*/

/*This solution is made for PostgreSQL 9.6*/

SELECT name, greeting, unnest((SELECT regexp_matches(greeting, '#(\d+)'))) AS user_id 
FROM greetings;

-- Another solution:
SELECT name, greeting, substring(greeting FROM '#(\d+)') AS user_id FROM greetings;



/* create a simple MIN / MAX statement that will return the Minimum and Maximum ages out of all the people.

people table schema
id
name
age
select table schema
age_min (minimum of ages)
age_max (maximum of ages)*/

SELECT MIN(age) AS age_min, MAX(age) AS age_max
FROM people;



/*Basic hello world without creating tables and column is on double
 quotes because PostgreSQL will turn it lower case otherwise*/
SELECT 'hello world!' AS "Greeting";




/*You are given a table 'random_string' that has the following format:

** random_string schema **

text
The text field holds a single row which contains a random string.

Your task is to take the random string and split it on each vowel (a, e, i, o, u) then the resultant substrings will be contained in the output table, formatted as:

** output table schema **

results
Note that the vowels should be removed.

If there are no vowels, there will only be one row returned. Where there are multiple vowels in succession, you will see empty rows. A row should ebe created on each break, whether there is content in the row or not.

Regex is advised but not mandatory.*/

SELECT regexp_split_to_table(text,'[aeiou]') AS results FROM random_string;



/*You are the owner of the Grocery Store. All your products are in the database, that you have created after CodeWars SQL excercises!:)

You have reached a conclusion that you waste to much time because you have to many different warehouse to visit each week.

You have to find out how many unique products have each of the Producer. If you take only few items from some of them you will stop going there to save the gasoline:)

In the results show producer and unique_products which you buy from him.

Order the result by unique_products (DESC) then by producer (ASC) in case there are duplicate amounts,

products table schema
id
name
price
stock
producer
results table schema
unique_products
producer*/

SELECT COUNT(name) AS unique_products, producer
FROM products
GROUP BY producer 
ORDER BY unique_products DESC, producer ASC;



/*You need to check what products are running out of stock, to know which you need buy in a CompanyA warehouse.

Use SELECT to show id, name, stock from products which are only 2 or less item in the stock and are from CompanyA.

Order the results by product id*/

SELECT id, name, stock
FROM products
WHERE stock < 3 AND producent = 'CompanyA'
ORDER BY id;

/*You are given a table with the following format:

** encryption table schema **

md5
sha1
sha256
Problem is the table looks so unbalanced - the sha256 column contains much longer strings. You need to balance things up. Add '1' to the end of the md5 addresses as many times as you need to to make them the same length as those in the sha256 column. Add '0' to the beginning of the sha1 values to achieve the same result.

Return as:

** output table schema **

md5
sha1
sha256*/

UPDATE encryption
SET md5 = RPAD(md5, LENGTH(sha256) , '1')
WHERE LENGTH(md5) < LENGTH(sha256);

UPDATE encryption
SET sha1 = LPAD(sha1, LENGTH(sha256) , '0')
WHERE LENGTH(sha1) < LENGTH(sha256);

SELECT md5, sha1, sha256
FROM encryption;

/*Without update*/

select RPAD(md5, length(sha256), '1') md5,
       LPAD(sha1, length(sha256), '0') sha1,
       sha256
  from encryption

/*There is an events table used to track different key activities taken on a website. For this task you need to filter the name field to only show "trained" events. Events should be grouped by the day they happened and counted. The description field is used to distinguish which items the events happened on.

"events" Table Schema
id
name (String)
created_at (DateTime)
description (String)
The expected results is provided so that you can see what the expected output is supposed to look like. Your "actual" output needs to match the expected output.*/

SELECT CAST(created_at AS DATE) AS day, description, COUNT(name)
FROM events
WHERE name = 'trained'
GROUP BY day, description
ORDER BY day asc;