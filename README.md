# coding_samples

Repo is a bit messy since I push various stuff here like codewars but I made unit testing and functional testing on weather_api folder as a reference.

There's also some RW stuff in folder unit-testing-with-robot-frameworks I did for learning purposes.

A lot of the stuff in other folders is protos I've made to get more familiar with Python. Codewars Python stuff has some light unit testing to make sure they pass the tests there and still a WiP.