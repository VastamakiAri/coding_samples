'''
Write a program which can compute the factorial of a given numbers.
The results should be printed in a comma-separated sequence on a single line.
Suppose the following input is supplied to the program:
8
Then, the output should be:
40320

Hints:
In case of input data being supplied to the question, it should be assumed to be a console input.
'''

def fractal_of_input(fractal):
    
    num = 1

    for i in range(1, fractal+1):

        if (i<=0):
            num = 1

        elif (i >= fractal):
            num *= fractal

        else:
            num *= i

    return num

if __name__ == '__main__':
    fractal = int(input("Give me fractal: "))
    print(fractal_of_input(fractal))