import random
a = random.sample(range(1,30), 12)
b = random.sample(range(1,30), 16)
list = []

overlap = [i for i in set(a) if i in b]

for i in overlap:
    if i not in list:
        list.append(i)

print(list)

'''
x = [1, 2, 3]
y = [5, 10, 15]
allproducts = [a*b for a in x for b in y]


x = [1, 2, 3]
y = [5, 10, 15]
customlist = [a*b for a in x for b in y if a*b%2 != 0]

#done in two list of comprehensions

import random
a = random.sample(range(1,30), 12)
b = random.sample(range(1,30), 16)
result_overlaps = [i for i in set(a) if i in b]
result = [i for i in result_overlaps if result_overlaps.count(i) == 1]

'''