x = (i for i in range(6))
#x = range(5) #has not __next__() because x is not a generator
next(x)
x.__next__()

for i in x:
    print(i)

print(dir(x))