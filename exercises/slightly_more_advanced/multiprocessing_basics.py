import multiprocessing
from multiprocessing import Pool

'''
Python is single threaded so it uses only one CPU even if you have more cores,
which means that on multicore CPU's you can only use a fraction of it's processing
power. With multiprocessing this can be bypassed.
'''
# Multiprocessing is a built-in Python library

# Basic example, with p = multiprocessing.Process(target=spawn, args=(i, i+1)) you init the
# multiprocessing, you can give it args and a target function. args have to be a tuple so
# single arg has to have a trailing "," e.g. args=(i,).
# To start use .start() and .join() makes it so processes wait for each other to end so
# remove if if you want processes to execute simultaneously.

def job(num):
    return num * 2

def spawn(num, num2):
    print('Spawn # {} {}'.format(num, num2))

if __name__ == '__main__':
    for i in range(5):
        p = multiprocessing.Process(target=spawn, args=(i, i+1))
        p.start()
        p.join()
    # Pool() allows allocation of processes to a function
    p = Pool(processes=20)
    data1 = p.map(job, [i for i in range(20)])
    data2 = p.map(job, [1, 2, 3, 4])
    p.close()
    print(data1)
    print(data2)

