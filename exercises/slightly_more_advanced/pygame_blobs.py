import pygame
import random
from blobs import Blob

STARTING_BLUE_BLOBS = 10
STARTING_RED_BLOBS = 5
WIDTH = 800
HEIGHT = 600
WHITE = (255, 255, 255)
BLUE = (0, 0, 255)
RED = (255, 0, 0)

game_display = pygame.display.set_mode((WIDTH,HEIGHT))
pygame.display.set_caption('Blob World')
clock = pygame.time.Clock()

class BlueBlob(Blob):

    def __init__(self, x_boundary, y_boundary):
        super().__init__((0, 0, 255), x_boundary, y_boundary)


class RedBlob(Blob):

    def __init__(self, x_boundary, y_boundary):
        super().__init__((255, 0, 0), x_boundary, y_boundary)


def draw_environment(blob_dict):
    game_display.fill(WHITE)
    for blob_list in blob_dict:
        for i in blob_list:
            blob = blob_list[i]
            pygame.draw.circle(game_display, blob.color, [blob.x,blob.y], blob.size)
            blob.move()
    pygame.display.update()
    
def main():
    blue_blobs = dict(enumerate(BlueBlob(WIDTH,HEIGHT) for i in range(STARTING_BLUE_BLOBS)))
    red_blobs = dict(enumerate(RedBlob(WIDTH,HEIGHT) for i in range(STARTING_RED_BLOBS)))
    while True:
        for event in pygame.event.get():
            # pylint: disable=no-member 
            if event.type == pygame.QUIT:
                pygame.quit()
                # pylint: enable=no-member
                quit()

        draw_environment([blue_blobs,red_blobs])
        clock.tick(60)

if __name__ == '__main__':
    main()