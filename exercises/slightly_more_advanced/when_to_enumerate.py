example = ['left','right','up','down']

# Instead of doing this:
for i in range(len(example)):
    print(i, example[i])
print()

# You should use enumerate()
for i,j in enumerate(example):
    print (i,j)
print()

# You can iterate through stuff like dictionaries with enumerate easily:
example_dict = {'left':'<','right':'>','up':'^','down':'v'}
[print(i,j) for i,j in enumerate(example_dict)]

# As a side note, you can also use dict and enumerate to create sort of unique ID:
new_dict = dict(enumerate(example))
print(new_dict)

# If you create a dictionary where the key is simply the index of the value in a list, just keep it as a list
# because if you take a slice out of it the numbers aren't going to match anymore