# Generally generator expressions are enough for Python and more Pythonic but it's
# a good idea to know how to build one in order to understand better how they work and
# to recognize one if needed

def simple_gen():
    yield 'Oh'
    yield 'hello'
    yield 'there'

[print(i) for i in simple_gen()]

# Here is a simple hard coded combination lock breaking iteration and after that a way to do it in generator:
CORRECT_COMBO = (3, 6, 1)
'''
found_combo = False
for c1 in range(10):
    if found_combo:
        break
    for c2 in range(10):
        if found_combo:
            break
        for c3 in range(10):
            if (c1, c2, c3) == CORRECT_COMBO:
                print('Found the combo:{}'.format((c1, c2, c3)))
                found_combo = True
                break
'''

# With gen:

def combo_gen():
    for c1 in range(10):
        for c2 in range(10):
            for c3 in range(10):
                yield (c1, c2, c3)

for (c1, c2, c3) in combo_gen():
    print(c1, c2, c3)
    if (c1, c2, c3) == CORRECT_COMBO:
        print('Found the combo:{}'.format((c1, c2, c3)))
        break

# Generators don't save entire list into memory at once so they're more easy to process but slower
# than list of comprehensions because of that
# Remember to use generator expressions instead for general use