students = [["Ben", {"Maths": 67, "English": 78, "Science": 72}],
            ["Mark", {"Maths": 56, "Art": 64, "History": 39, "Geography": 55}],
            ["Paul", {"English": 66, "History": 88}]]

grades = ((0, "FAIL"),(50, "D"),(60,"C"),(70, "B"), (80,"A"), (101, "CHEAT!"))

print(grades[0])
'''
for student in students:
    print("Report card for student ", student[0])            
    for subject, mark in student[1].items():
        for grade in grades:
            if mark < grade[0]:
                prev_grade = grade[1]
                print(subject, " : ", prev_grade)
                break
'''