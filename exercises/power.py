def power(base, exponent):
    result = base**exponent
    return result

if __name__ == '__main__':
    base = input("Insert base")
    exponent = input("Insert exponent")
    result = power(base,exponent)
    print "%d to the power of %d is %d" %(base, exponent, result)