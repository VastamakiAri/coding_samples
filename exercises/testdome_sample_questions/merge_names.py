def unique_names(names1, names2):
    mergedlist = list(set(names1 + names2))
    return mergedlist

names1 = ["Ava", "Emma", "Olivia"]
names2 = ["Olivia", "Sophia", "Emma"]
print(unique_names(names1, names2)) # should print Ava, Emma, Olivia, Sophia