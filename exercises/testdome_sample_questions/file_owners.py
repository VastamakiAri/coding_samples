def group_by_owners(files):

    inv_map = {}
    for key, value in files.items():
        inv_map[value] = inv_map.get(value, [])
        inv_map[value].append(key)
    '''
    inv_map = dict(zip(files.values(), files.keys()))
    '''        
    return inv_map
    
files = {
    'Input.txt': 'Randy',
    'Code.py': 'Stan',
    'Output.txt': 'Randy'
}   
print(group_by_owners(files))