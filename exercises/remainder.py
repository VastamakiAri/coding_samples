n = 8

for k in range(2,n):
    print("{} / {} = {}, remainder {}".format(n,k,n // k, n % k))

#goes until n % k = 0
for n in range (198, 3003):
    b = True
    k = 2
    while b and k < n:
        #print("{} / {} = {}, remainder {}".format(n,k,n // k, n % k))
        if n % k == 0:
            b = False
        k = k + 1

    if b:
        print(n)