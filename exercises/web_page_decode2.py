from bs4 import BeautifulSoup
import requests
import sys

url = 'http://www.vanityfair.com/society/2014/06/monica-lewinsky-humiliation-culture'
r = requests.get(url)
r.encoding = 'utf-8'
r_html = r.text

soup = BeautifulSoup(r_html, "html.parser")

for story_text in soup.find_all("p"):
    print(story_text.text.encode('utf8'))


'''
for story_heading in soup.find_all(class_="story-heading"): 
    if story_heading.a: 
        print(story_heading.a.text.replace("\n", " ").strip().encode('utf8'))
    else: 
        print(story_heading.contents[0].strip().encode('utf8'))
'''