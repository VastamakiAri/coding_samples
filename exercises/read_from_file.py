darth = 0
lea = 0
luke = 0

with open ('file_to_read.txt','r') as open_file:
    lines = open_file.read()

    for line in lines.split('\n'):

        if line == 'Darth':
            darth += 1

        elif line == 'Lea':
            lea += 1

        elif line == 'Luke':
            luke += 1

print("Amount of Lukes: %d, Leias: %d and Darths: %d In a file" % (luke, lea, darth))


#better way with dictionary

counter_dict = {}
with open('file_to_read.txt') as f:
	line = f.readline()
	while line:
		line = line.strip()
		if line in counter_dict:
			counter_dict[line] += 1
		else:
			counter_dict[line] = 1
		line = f.readline()

print(counter_dict)