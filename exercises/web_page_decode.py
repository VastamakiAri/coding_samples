from bs4 import BeautifulSoup
import requests

url = 'http://www.nytimes.com/'
r = requests.get(url)
r_html = r.text

soup = BeautifulSoup(r_html, "html.parser")

with open('file_to_save.txt', 'w') as open_file:

    for story_heading in soup.find_all(class_="story-heading"): 
        if story_heading.a: 
            open_file.write(story_heading.a.text.replace("\n", " ").strip().encode('utf8'))
            open_file.write("\n")

        else: 
            open_file.write(story_heading.contents[0].strip().encode('utf8'))
            open_file.write("\n")