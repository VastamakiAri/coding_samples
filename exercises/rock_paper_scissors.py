import random
import sys

def game(player, cpu):

    if  (player == "rock" and cpu == "scissors"):
        return("Player wins with rock!")

    elif player == "paper" and cpu == "rock":
        return("Player wins with paper!")

    elif player == "scissors" and cpu == "paper":
        return("Player wins with scissors!")

    if  (cpu == "rock" and player == "scissors"):
        return("Cpu wins with rock!")

    elif cpu == "paper" and player == "rock":
        return("Cpu wins with paper!")

    elif cpu == "scissors" and player == "paper":
        return("Cpu wins with scissors!")
    
    elif player == cpu:
        return("It's a tie!")

    else:
        print("Thank you for playing!")
        print("...")
        sys.exit()
        
#rock > scissors > paper > rock
table = ['rock', 'paper', 'scissors']

while True:
    player = str(input("rock, paper scissors? <press enter to quit> "))
    cpu = random.choice(table)
    print(game(player, cpu))



