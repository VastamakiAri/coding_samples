#program prints common numbers between prime and happy numbers 
# up to 1000th number

def file_to_int(text_file):
    text = []
    with open (text_file,'r') as open_file:
        line = open_file.readline()
        while line:
            text.append(int(line))
            line = open_file.readline()
        return text

primes = file_to_int("primenumbers.txt")
happies = file_to_int("happynumbers.txt")

overlap = [i for i in primes if i in happies]
print(overlap)