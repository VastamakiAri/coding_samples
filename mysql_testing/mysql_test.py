# MySQL syntax testing for Python
import os
import mysql.connector
import datetime
from ConfigParser import SafeConfigParser

location_of_files = 'C:\\Users\\Ari\\Oracle'
file_name = 'mysql.cnf'
parser = SafeConfigParser()
parser.read(os.path.join(location_of_files,file_name))
hostname = parser.get('mysql','hostname')
username = parser.get('mysql','username')
password = parser.get('mysql','password')
database = parser.get('mysql','database')

cnx = mysql.connector.connect(user=username, 
                              password=password,
                              host=hostname,
                              database=database)

cursor = cnx.cursor()

query = ("SELECT first_name, last_name, birth_day FROM employee "
          "WHERE sex = %s AND birth_day > %s")

data = ('F', datetime.date(1971,1,1))

cursor.execute(query, data)

for (first_name, last_name, birth_day) in cursor:
  print("{}, {} birthday is on {:%d %b %Y}".format(
  last_name, first_name, birth_day))

cursor.close()
cnx.close()

'''
Example print:
Martin, Angela birthday is on 25 Jun 1971
Kapoor, Kelly birthday is on 05 Feb 1980
Beesly, Pam birthday is on 21 Mar 1979
'''  