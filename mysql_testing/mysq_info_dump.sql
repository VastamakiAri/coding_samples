-- 
INT				--whole numbers
DECIMAL(M,N)	--decimal numbers - exact value
VARCHAR(l)		--string of text of length l
BLOB			--binary large object, stores large data
DATE			--'YYYY-MM-DD'
TIMESTAMP		--'YYYY-MM-DD HH:MM:SS' -used for recording date;
-- 
-- SAMPLE INSERTS --

CREATE TABLE student (
    student_id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(25) NOT NULL,
    major VARCHAR(20) DEFAULT 'undecided'
);


DESCRIBE student;

DROP TABLE student;

ALTER TABLE student ADD gpa DECIMAL(3,2);

ALTER TABLE student DROP COLUMN gpa;


INSERT INTO student(name,major) VALUES('Mike','Computer Science');

INSERT INTO student(name,major) VALUES(
    'Jack', 'Biology'
);

INSERT INTO student(name,major) VALUES('Jill','Chemistry');


INSERT INTO student(student_id,name) VALUES(1,'Kate');

SELECT * 
FROM STUDENT;

UPDATE student
SET major = 'Biology'
WHERE major = 'Bio';

UPDATE student
SET student_id = '1' name ='Tom', major='undecided'
WHERE student_id='1';

UPDATE student
SET major='undecided'

DELETE FROM student;

DELETE FROM student
WHERE name = 'Tom' AND major = 'undecided';

-- QUERIES --

SELECT student.name, student.major
FROM student
ORDER BY name, student_id ASC
LIMIT 3;

SELECT *
FROM student
WHERE major = 'Biology' OR major = 'Computer Science';

-- <, >, <=, >=, =, <>, AND, OR

SELECT *
FROM student
WHERE name IN ('Claire', 'Kate', 'Mike');

SELECT *
FROM student
WHERE major IN ('Biology', 'Chemistry') AND student_id > 2;



-- TRIGGERS --

DROP TABLE trigger_test;
DROP TRIGGER my_trigger;

CREATE TABLE trigger_test (
    message VARCHAR(100)

);

DESCRIBE trigger_test;

-- Add this into MySQL command line instead of here

DELIMITER $$ -- MYSQL command it changes the ender from ";" into "$$"
CREATE
    TRIGGER my_trigger BEFORE INSERT
    ON employee
    FOR EACH ROW BEGIN
        INSERT INTO trigger_test VALUES(NEW.first_name);
    END $$
DELIMITER ;    -- changes it back
;


DELIMITER $$ -- MYSQL command it changes the ender from ";" into "$$"
CREATE
    TRIGGER my_trigger2 BEFORE INSERT
    ON employee
    FOR EACH ROW BEGIN
        IF NEW.sex = 'M' THEN
            INSERT INTO trigger_test VALUES('added male employee');
        ELSEIF NEW.sex = 'F' THEN
            INSERT INTO trigger_test VALUES('added female employee');
        ELSE
            INSERT INTO trigger_test VALUES('added other employee');
        END IF;
    END $$
DELIMITER ;  

;


INSERT INTO employee
VALUES (110, 'Kevin', 'Malone','1978-02-19', 'M',  69000, 106, 3);

INSERT INTO employee
VALUES (111, 'Pam', 'Beesly','1979-03-21', 'F',  70000, 106, 3);



SELECT *
FROM trigger_test;

UPDATE employee
SET sex = 'F'
WHERE first_name = 'Pam';

DELETE FROM employee
WHERE first_name = 'Pam' AND sex = 'F';

