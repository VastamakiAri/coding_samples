import unittest
import weather_api.forecast as forecast
import xmlrunner

class WeatherApiFunctionalTests(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_forecast(self):
        city = forecast.request_call('Isokyrö')
        result = forecast.weather(city)
        self.assertTrue(result)
  
    def test_incorrect_string(self):
        city = forecast.request_call('Isok')
        self.assertFalse(forecast.weather(city))

    def test_empty_string(self):
        city = forecast.request_call('')
        #with self.assertRaises(KeyError):
        #   forecast.weather(city)
        self.assertFalse(forecast.weather(city))

    def test_numeral(self):
       with self.assertRaises(TypeError):
            city = forecast.request_call(535)
            forecast.weather(city)
            
if __name__ == '__main__':
    unittest.main(testRunner=xmlrunner.XMLTestRunner(output='test-reports'),
        failfast=False, buffer=False, catchbreak=False)