import unittest
import weather_api.forecast as forecast
from forecast import key_parser
from unittest.mock import patch
import xmlrunner

class WeatherApiUnittests(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.appid = key_parser()

    def test_request_http_status(self):
        city = 'Tampere'
        response = forecast.requests.get('http://api.openweathermap.org/data/2.5/weather?q='+city+'&units=metric&APPID='+self.appid)
        self.assertEqual(response.status_code, 200)

    def test_request_response(self):
        # Send a request to the API server and store the response.
        city = 'Helsinki'
        response = forecast.requests.get('http://api.openweathermap.org/data/2.5/weather?q='+city+'&units=metric&APPID='+self.appid)
        self.assertTrue(response.ok)

    @patch('forecast.requests.get', autospec=True)
    def test_request_call(self, mocked_get):
        mocked_get.return_value.ok = True
        response = mocked_get('')
        self.assertIsNotNone(response)

    def test_weather_print(self):
        pass
    
    @patch('forecast.requests.get', autospec=True)
    def test_int(self, mocked_get):
        city = 484           
        with self.assertRaises(TypeError):
            mocked_get('http://api.openweathermap.org/data/2.5/weather?q='+city+'&units=metric&APPID='+self.appid)
            mocked_get.assert_called_with('http://api.openweathermap.org/data/2.5/weather?q='+city+'&units=metric&APPID='+self.appid)
            mocked_get.assert_called_once()

    @patch('forecast.requests.get', autospec=True)
    def test_incorrect_string(self, mocked_get):
        mocked_get.return_value = {'cod': '404', 'message': 'city not found'}
        with self.assertRaises(KeyError):
            forecast.weather(mocked_get.return_value)

    @unittest.skip("demonstrating skipping")
    def test_no_args(self):
        with self.assertRaises(SystemExit):
            forecast.main()

if __name__ == '__main__':
    unittest.main(testRunner=xmlrunner.XMLTestRunner(output='test-reports'),
        failfast=False, buffer=False, catchbreak=False)