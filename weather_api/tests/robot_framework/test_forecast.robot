*** Settings ***
Resource        test_forecast_keywords.robot

*** Test Cases ***
Get Requests
    [tags]      request_functionality
    Create Session	    openweather	${url}		
    ${response}         Acquiring JSON Data
    Verifying Request   ${response}

Forecast Smoke Test
    [tags]      request_call
    Forecast Call  Vaasa

Request Non Existing City
    [tags]      request_call
    Incorrect City String Call  Isok
    Incorrect City String Call  " "
    Incorrect City String Call  555