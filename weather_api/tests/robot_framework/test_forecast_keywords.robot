*** Settings ***
Library	        Collections			
Library	        RequestsLibrary
Library         forecast.py
#Suite Setup     Key Parser

*** Variables ***
${url}      http://api.openweathermap.org/
${city}     Vaasa    

*** Keywords ***
Acquiring JSON Data
    ${appid}                Key Parser
    Should Not Be Empty     ${appid}
    ${resp} 	            Get Request	openweather	/data/2.5/weather?q=${city}&units=metric&APPID=${appid}
    [Return]                ${resp}

Verifying Request
    [Arguments]     ${resp}
    Should Be Equal As Strings	        ${resp.status_code}	200
    Log                                 ${resp.json()}
    Dictionary Should Contain Value	    ${resp.json()}	${city}

Forecast Call
    [Arguments]     ${city}
    ${result}       Request Call  ${city}
    Log             ${result}
    Dictionary Should Contain Value	    ${result}	${city} 
    #Should Be True  ${result}

Incorrect City String Call
    [Arguments]     ${city}
    ${result}       Request Call  ${city}
    Log             ${result}
    Should Be Equal  ${result}  ${None}

Request Call With Integer
    [Arguments]     ${city}
    ${response}     Request Call  ${city}
    Log             ${response}
    Run Keyword And Expect Error  TypeError:*  Weather ${city}
