import sys
import requests
import logging
import os
from configparser import ConfigParser
#from pprint import pprint

# -----------------------------------------------------------------------
# Simple program that prints temperature of a city
# City id's can be found in city.list.json
# Api documentation: https://openweathermap.org/guide
# Usage: python .\forecast.py <city_name>
# -----------------------------------------------------------------------

# Suggestions #
#-------------------------------------------------------------------------
# Would be interesting to test out binary search with city.list.json
# Should test argparse() variation
#-------------------------------------------------------------------------

#logging.basicConfig(level=logging.INFO)
logging.basicConfig(filename='logfile.log',level=logging.INFO)

def main(argv):
    if len(argv) < 2:
        sys.stderr.write('Usage: python .\\forecast.py <city_name>')
        return 1
        
    else:
        city = sys.argv[1]
        weather_data = request_call(city)
        weather(weather_data)

def error_handling():
    return '{}. {}, line: {}'.format(sys.exc_info()[0],
                                    sys.exc_info()[1],
                                    sys.exc_info()[2].tb_lineno)

def key_parser():
    file_location = 'C:\\Users\\Ari\\OpenWeather'
    file_name = 'weather.cnf'
    parser = ConfigParser()
    parser.read(os.path.join(file_location,file_name))
    appid = parser.get('weather','appid')
    return appid

def request_call(city):
    appid = key_parser()

    try:
        r = requests.get('http://api.openweathermap.org/data/2.5/weather?q='+city+'&units=metric&APPID='+appid)

    except TypeError as te:
        logging.error(error_handling())
        raise te

    except:
        sys.exit('Usage: python .\\forecast.py <city_name>')

    if r.ok:
        return r.json()
    else:
        return None

def weather(weather_data):
    m_symbol = u'\xb0' + 'C'

    if weather_data == None:
        print('city not found')
        return False

    try:
        #print ("\n----------------------------------------")
        print ('{}, {}'.format(weather_data['name'], weather_data['sys']['country']))
        print ('Weather: {}'.format(weather_data['weather'][0]['description']))
        print ('Current temperature: {:.2f} {}'.format(weather_data['main']['temp'], m_symbol))
        print ('Temperature range: {:.2f} {} - {:.2f} {}'.format(weather_data['main']['temp_min'], m_symbol, weather_data['main']['temp_max'], m_symbol))
        print ('Wind speed: {:.1f} m/s'.format(weather_data['wind']['speed']))
        print ('Humidity: {} %'.format(weather_data['main']['humidity']))
        #print ("----------------------------------------")
        return weather_data

    except KeyError as ke:
        logging.error(error_handling())
        raise ke

    except TypeError as te:
        logging.error(error_handling())
        raise te

    except Exception as e:
        logging.error(error_handling())
        raise e

if __name__ == '__main__':
    sys.exit(main(sys.argv))

# Isokyros information from city.list.json as an example
#  {
#    "id": 656456,
#    "name": "Isokyro",
#    "country": "FI",
#    "coord": {
#      "lon": 22.333321,
#      "lat": 63.011719
#    }
#  },

# Sample of weathermaps JSON structure
'''
{u'base': u'stations',
 u'clouds': {u'all': 0},
 u'cod': 200,
 u'coord': {u'lat': 63, u'lon': 22.33},
 u'dt': 1551802699,
 u'id': 656457,
 u'main': {u'humidity': 67,
           u'pressure': 1000,
           u'temp': -9.13,
           u'temp_max': -6.67,
           u'temp_min': -11.11},
 u'name': u'Isokyr\xf6',
 u'sys': {u'country': u'FI',
          u'id': 1367,
          u'message': 0.0045,
          u'sunrise': 1551763286,
          u'sunset': 1551801846,
          u'type': 1},
 u'visibility': 10000,
 u'weather': [{u'description': u'clear sky',
               u'icon': u'01n',
               u'id': 800,
               u'main': u'Clear'}],
 u'wind': {u'deg': 360, u'speed': 4.1}}
 '''

# Output formatted with pprint()
#pprint(r.json())
#print ('\n')