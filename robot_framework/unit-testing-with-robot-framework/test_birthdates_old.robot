#*** WiP need to make keywords for test cases, data driven version (test_birthdates.robot) is more updated

*** Settings ***
Library     birthdays.py
Library   OperatingSystem

*** Variables ***   
${name}         Ari 
${date}         5.8.1990 
${path}         user_info.json 

** Test Cases **

Initialization test
    [Documentation]     Making sure JSON file is loaded properly into the dictionary variable birthdays[]
    ${birthdays} =      Bday Init  ${path}
    ${json} =           Get file   ${path}
    ${json} =  Evaluate   json.loads("""${json}""")    json
    Should Be Equal As Strings  ${birthdays}  ${json}

Updating json
    [Documentation]    Checks if name was added properly into JSON
    #${birthdays} =  Bday Init  ${path}
    Update Json  ${name}  ${date}  ${path}
    ${json} =  Get file   ${path}
    ${json} =  Evaluate   json.loads("""${json}""")    json
    Should Contain  ${json}  ${name}

Show information of one person
    [Documentation]     Testing name search function
    ${result} =  Show Persons Info  ${name}
    Should Be Equal As Strings  ${result}  Ari

Remove someone
    [Documentation]     Testing name removal function
    Remove Person  ${name}  ${path}
    ${json} =  Get file   ${path}
    Should Not Contain  ${json}  ${name}


*** Keywords ***

Initialization
    #[Arguments]         ${path} ${expected}      
    ${birthdays} =      Bday Init  ${path}
    #${json} =           Get file   ${path}
    #Should Be Equal As Strings    ${birthdays}  ${json}