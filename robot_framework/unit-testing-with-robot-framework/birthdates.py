from __future__ import print_function
import json
import datetime
from collections import Counter

# Simple JSON information into dictionary software which stores names and birthdates.
# It has tests done with Robot Framework in file data_driven_test_birthdates.robot 

# To Do 
# use something like enumerate() so that program can accept multiple people (unique ID's) with same name instead of overwriting akin dictionary inside a list
# add made up social security numbers and maybe remove enumerate after
# remove comments that are unrelated
# add comments to modules
# error check improvements

PATH = "user_info.json"
BACKUP_PATH = "backup.json"

# Used for testing but also works as backup
def load_initial_JSON_state(path, backup_path):

    try:        
        with open(path, "r") as r:
            bdate = json.load(r)

    except IOError as exc:
        raise IOError("%s: %s" % (path, exc.strerror))

    try:        
            with open(backup_path, "w") as w:
                json.dump(bdate, w)

    except IOError as exc:
        raise IOError("%s: %s" % (backup_path, exc.strerror))

def datetime_handler(x):
    if isinstance(x, datetime.date):
        return x.isoformat()
    raise TypeError("Unknown type")

def bday_init(path):

    try:        
        with open(path, "r") as r:
            bdays = json.load(r)
        return bdays

    except IOError as exc:
        raise IOError("%s: %s" % (path, exc.strerror))

def menu():
    print("\n----------This program prints person and their birthday from JSON file----------")
    print("Enter 1 to view all")
    print("Enter 2 to view one persons info")
    print("Enter 3 to see month distribution")
    print("Enter 4 to add a person")
    print("Enter 5 to remove someone")
    print("Enter q to exit")
    print("--------------------------------------------------------------------------------")

def update_json(name, bday, path):
    
    day, month, year = map(int, bday.split('.'))
    x = datetime.date(year, month, day)
    str_converted_date = datetime_handler(x)
    birthdays = bday_init(path)   
    birthdays[name] = str_converted_date

    try:        
        with open(path, "w") as w:
            json.dump(birthdays, w)

    except IOError as exc:
        raise IOError("%s: %s" % (path, exc.strerror))

    print("%s added into the dictionary"%(name))
    return name

def count_months(path):

    birthdays = bday_init(path)

    num_to_string = {
	1: "January",
	2: "February",
	3: "March", 
	4: "April",
	5: "May",
	6: "June",
	7: "July",
	8: "August",
	9: "September",
	10: "October",
	11: "November",
	12: "December"
    }
    
    months = []

    for x in birthdays.values():
        month = int(x.split("-")[1])
        months.append(num_to_string[month])
        #months.sort(key=months[1])
        
    print(Counter(months))
    return months

def show_persons_info(path, choice):

    birthdays = bday_init(path)
       
    if choice in birthdays.keys():
        print("%s's date of birth is %s" % (choice, birthdays[choice]))
        return choice
    else:
        print("name not found")
        
def remove_person(choice, path):

    birthdays = bday_init(path)

    try:
        del birthdays[choice]

        try:        
            with open(path, "w") as w:
                json.dump(birthdays, w)
            print("%s removed from the dictionary"%(choice))
            return choice

        except IOError as exc:
            raise IOError("%s: %s" % (path, exc.strerror))

    except KeyError:
        print("not in the dictionary")

if __name__ == '__main__':


    load_initial_JSON_state(PATH, BACKUP_PATH)    
    menu()

    while True:

        choice = str(raw_input(""))
        
        if choice == "1":
            birthdays = bday_init(PATH)
            for name in birthdays:
                print("%s, %s"%(name, birthdays[name]))

        elif choice == "2":
            person_info = (str(raw_input("Whose birthday do you want to know? ")))
            show_persons_info(PATH, person_info)     

        elif choice == "3":
            count_months(PATH)

        elif choice == "4":
            name = str(raw_input("Give full name: "))
            date = str(raw_input("Enter date of birth in DD.MM.YYYY format "))
            update_json(name, date, PATH)

        elif choice == "5":
            del_person = (str(raw_input("Who do you want to delete? ")))
            remove_person(del_person, PATH)

        elif choice == "q":
            load_initial_JSON_state(PATH, BACKUP_PATH)
            print("Thank you for using this program")
            raise SystemExit
            
        else:
            menu()