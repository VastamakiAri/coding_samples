# Robot Framework unit testing for birthdates.py
# TODO 
# Remove comments that are unrelated
# Add some more tests

*** Settings ***
#Test Template   Test
#Test Setup     Load JSON test data
Suite Setup     Initialization test
Test Setup      Load initial JSON state  ${backup}  ${path}    
Library         birthdates.py
Library         OperatingSystem

*** Variables ***   
${path}          user_info.json  
${backup}        backup.json
                    

*** Test Cases ***      name            **date            

Adding Person           [Template]      Updating json 
                        Jean            14.12.1899               
                        Masters         20.5.2011
                        Ari             5.8.1990

Remove person           [Template]      Remove someone
                        Ari  

Show person             [Template]      Show information of one person
                        Ari       



      
#Datetime handler        
#    Handler  ${expected}

*** Keywords ***

Initialization test
    [Documentation]     Making sure JSON file is loaded properly into the dictionary variable birthdate[]
    ${birthdate} =      Bday Init  ${path}
    ${json} =           Get file   ${path}
    ${json} =           Evaluate   json.loads("""${json}""")    json
    Should Be Equal As Strings  ${birthdate}  ${json}

Updating json
    [Arguments]     ${name}  ${date}  
    Update Json     ${name}  ${date}  ${path}
    ${json} =       Get file   ${path}
    ${json} =       Evaluate   json.loads("""${json}""")    json
    Should Contain  ${json}  ${name}

Show information of one person
    [Arguments]     ${name}
    ${result} =     Show Persons Info  ${path}  ${name} 
    Should Be Equal As Strings  ${result}  ${name}

Remove someone
    [Arguments]    ${name}   
    Remove Person  ${name}   ${path}
    ${json} =      Get file  ${path}
    Should Not Contain  ${json}  ${name}



#Counting months
#    bday_init
#    ${result} = count_months
#    Should Be Equal As Integers  ${result}  16

#Handler
#   [Arguments]   ${expected}
#    ${date} =  Datetime Handler  ${expected}
#    Should Be Equal As Strings  ${date}  ${expected}
