import numpy as np
from scipy import stats
import matplotlib.pyplot as plt

n = 10
p = 0.30
k = np.arange(stats.binom.ppf(0.01, n, p),
              stats.binom.ppf(0.99, n, p))
binomial = stats.binom.pmf(k,n,p)

x = np.arange(0,10)

cumulative = stats.binom.cdf(x, n, p, loc=0)
survival_function = stats.binom.sf(x, n, p, loc=0)
print("------------------------------------------------")
print("Cumulative probability of X <= x\n{}".format(list(np.around(np.array(cumulative),10))))
print("------------------------------------------------")
print("Cumulative probability of X >= x\n{}".format(list(np.around(np.array(survival_function),10))))
print("------------------------------------------------")
#myList = list(np.around(np.array(myList),2))

plt.plot(k,binomial,'o-')
plt.title('Binomial: n={}, p={:.2f}'.format(n,p), fontsize=15)
plt.xlabel('Number of successes')
plt.ylabel('Probability of successes', fontsize=15)
plt.show()